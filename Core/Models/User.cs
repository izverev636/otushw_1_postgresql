﻿namespace Core.Models
{
    public class User : BaseEntity
    {
        public string Login { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public int Age { get; set; }
    }
}
