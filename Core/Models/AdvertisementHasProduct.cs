﻿using System;
namespace Core.Models
{
    public class AdvertisementHasProduct: BaseEntity
    {
        public Advertisement Advertisement { get; set; }

        public Product Product { get; set; }
    }
}

