﻿using System;
namespace Core.Models
{
    public class Product : BaseEntity
    {
        public string Productname { get; set; }

        public string? Description { get; set; }

        public decimal Price { get; set; }
    }
}

