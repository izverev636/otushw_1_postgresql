﻿using System;
namespace Core
{
    public interface IRepository<T> where T : BaseEntity
    {
        public Task<IEnumerable<T>> GetAllAsync();

        public Task<T> GetByIdAsync(int id);

        public void AddAsync(T Entity);

        public void DeleteAsync(T entity);

        public void SaveChangesAsync();
    }
}

