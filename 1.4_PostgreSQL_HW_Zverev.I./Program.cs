﻿using Core;
using Core.Models;
using DAL;
using DAL.Data;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.


var connection = builder.Configuration.GetConnectionString("PostgreConnectionString");
AvitoDataContextScripts.Init(connection);
builder.Services.AddDbContext<AvitoDataContext>(opt => opt.UseNpgsql(connection));

builder.Services.AddTransient<IRepository<Product>, ProductRepository>();
builder.Services.AddTransient<IRepository<Advertisement>, AdvertisementRepository>();
builder.Services.AddTransient<IRepository<AdvertisementHasProduct>, AdvertisementHasProductRepository>();
builder.Services.AddTransient<IRepository<User>, UserRepository>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
