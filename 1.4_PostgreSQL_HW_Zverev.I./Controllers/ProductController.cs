﻿using System.Net;
using Core;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _1._4_PostgreSQL_HW_Zverev.I_.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductController : ControllerBase
{
    private readonly IRepository<Product> _repository;

    public ProductController(IRepository<Product> repo)
    {
        _repository = repo;
    }

    [HttpGet(Name = "GetAllProduct")]
    public async Task<IEnumerable<Product>> GetAllAdvertisementsAsync()
    {
        return await _repository.GetAllAsync();
    }


    [HttpPost(Name = "AddNewProduct")]
    public async Task<HttpStatusCode> AddNewProductAsync([FromBody] Product product)
    {
        if (product != null)
        {
            _repository.AddAsync(product);
            _repository.SaveChangesAsync();

            return HttpStatusCode.Created;
        }
        return HttpStatusCode.BadRequest;
    }
}

