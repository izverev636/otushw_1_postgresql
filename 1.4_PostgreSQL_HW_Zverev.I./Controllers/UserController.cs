﻿using Core;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _1._4_PostgreSQL_HW_Zverev.I_.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly IRepository<User> _repository;

    public UserController(IRepository<User> repo)
    {
        _repository = repo;
    }

    [HttpGet(Name = "GetAllUser")]
    public async Task<IEnumerable<User>> GetAllUserAsync()
    {
        return await _repository.GetAllAsync();
    }
}

