﻿using Core;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _1._4_PostgreSQL_HW_Zverev.I_.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class AdvertisementController : ControllerBase
{
    private readonly IRepository<Advertisement> _repositoryA;

    private readonly IRepository<AdvertisementHasProduct> _repositoryAHP;

    public AdvertisementController(IRepository<AdvertisementHasProduct> repoAHP, IRepository<Advertisement> repoA)
    {
        _repositoryA = repoA;
        _repositoryAHP = repoAHP;
    }

    [HttpGet(Name = "GetAllAdvertisement")]
    public async Task<IEnumerable<Advertisement>> GetAllAdvertisementsAsync()
    {
        return await _repositoryA.GetAllAsync();
    }

    [HttpGet(Name = "GetAllAdvertisementLink")]
    public async Task<IEnumerable<AdvertisementHasProduct>> GetAllAdvertisementLink()
    {
        return await _repositoryAHP.GetAllAsync();
    }
}
