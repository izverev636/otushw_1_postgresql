﻿using System;
using System.Reflection;
using Core.Models;
using Npgsql;

namespace DAL.Data
{
    public static class AvitoDataContextScripts
    {
        public static void Init(string connString)
        {
            DropTables(connString);
            InitCreateTables(connString);
            InitInsertData(connString);
        }


        private static void DropTables(string connString)
        {
            string script =
                "DROP TABLE IF EXISTS public.AdvertisementHasProducts;" +
                "DROP TABLE IF EXISTS public.Advertisements;" +
                "DROP TABLE IF EXISTS public.Products;" +
                "DROP TABLE IF EXISTS public.Users;";

            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                var cmd = new NpgsqlCommand(script, conn);
                cmd.ExecuteNonQuery();
            }
        }

        private static void InitCreateTables(string connString)
        {
            string script = string.Empty;
            var assembly = Assembly.GetExecutingAssembly();

            using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream("DAL.CreateDatabaseScript.sql")))
            {
                script = (reader.ReadToEnd());
            }

            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                var cmd = new NpgsqlCommand(script, conn);
                cmd.ExecuteNonQuery();
            }
        }

        private static void InitInsertData(string connString)
        {
            var usrLst = GetUsersFakeData();
            foreach(User user in usrLst)
            {
                InsertToUsersTable(connString, user.Id, user.Login,
                    user.Firstname, user.Lastname,user.Age);
            }

            var productLst = GetProductsFakeData();
            foreach(Product product in productLst)
            {
                InsertToProductsTable(connString, product.Id, product.Productname,
                    product.Description, product.Price);
            }

            var AdvertisementsLst = GetAdvertisementsFakeData();
            foreach(Advertisement advertisement in AdvertisementsLst)
            {
                InsertToAdvertisementsTable(connString, advertisement.Id);
            }
            
            InsertToAdvertisementHasProductsTable(connString, 1, 1, 1);
            InsertToAdvertisementHasProductsTable(connString, 2, 1, 2);
            InsertToAdvertisementHasProductsTable(connString, 3, 2, 3);
            InsertToAdvertisementHasProductsTable(connString, 4, 3, 1);
            InsertToAdvertisementHasProductsTable(connString, 5, 4, 6);

        }

        private static void InsertToUsersTable(string connString, int id,
            string login, string firstname, string lastname, int age)
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                var script = @"
                          INSERT INTO public.users(
	                      id, login, firstname, lastname, age)
	                      VALUES (:id, :login, :firstname, :lastname, :age);";
                var cmd = new NpgsqlCommand(script, conn);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("id", id));
                parameters.Add(new NpgsqlParameter("login", login));
                parameters.Add(new NpgsqlParameter("firstname", firstname));
                parameters.Add(new NpgsqlParameter("lastname", lastname));
                parameters.Add(new NpgsqlParameter("age", age));
                cmd.ExecuteNonQuery();
            }
            
        }

        private static void InsertToProductsTable(string connString, int id,
            string productname, string description, decimal price)
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                var script = @"
                          INSERT INTO public.products(
	                      id, productname, description, price)
	                      VALUES (:id, :productname, :description, :price);";
                var cmd = new NpgsqlCommand(script, conn);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("id", id));
                parameters.Add(new NpgsqlParameter("productname", productname));
                parameters.Add(new NpgsqlParameter("description", description));
                parameters.Add(new NpgsqlParameter("price", price));
                cmd.ExecuteNonQuery();

            }
        }

        private static void InsertToAdvertisementsTable(string connString, int id)
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                var script = @"
                          INSERT INTO public.advertisements(
	                      id, datecreated)
	                      VALUES (:id, :datecreated);";
                var cmd = new NpgsqlCommand(script, conn);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("id", id));
                parameters.Add(new NpgsqlParameter("datecreated", DateTime.UtcNow));
                cmd.ExecuteNonQuery();
            }
        }

        private static void InsertToAdvertisementHasProductsTable(string connString, int id,
            int advertisementid, int productid)
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                var script = @"
                          INSERT INTO public.advertisementhasproducts(
	                      id, advertisementid, productid)
	                      VALUES (:id, :advertisementid, :productid);";
                var cmd = new NpgsqlCommand(script, conn);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("id", id));
                parameters.Add(new NpgsqlParameter("advertisementid", advertisementid));
                parameters.Add(new NpgsqlParameter("productid", productid));
                cmd.ExecuteNonQuery();
            }
        }

        private static IEnumerable<User> GetUsersFakeData()
        {
            var lst = new List<User>()
            {
                    new User
                    {
                        Id = 1,
                        Firstname = "Ivan",
                        Lastname = "Ivanov",
                        Age = 25,
                        Login = "ivan.invanov"
                    },
                    new User
                    {
                        Id = 2,
                        Firstname = "Alex",
                        Lastname = "Kozlov",
                        Age = 42,
                        Login = "alex.koz"
                    },
                    new User
                    {
                        Id = 3,
                        Firstname = "Roma",
                        Lastname = "Orlov",
                        Age = 14,
                        Login = "orelRoman2004"
                    },
                    new User
                    {
                        Id = 4,
                        Firstname = "Albert",
                        Lastname = "Petrov",
                        Age = 30,
                        Login = "ScruDriwer22"
                    },
                    new User
                    {
                        Id = 5,
                        Firstname = "Irina",
                        Lastname = "Are",
                        Age = 65,
                        Login = "OldWoman65"
                    } };

            return lst;
            }

        private static IEnumerable<Product> GetProductsFakeData()
        {
            var lst = new List<Product>()
            {
                new Product {
                    Id = 1,
                    Productname = "Старый зонтик",
                    Description = "Зеленый зонтик в хорошем состояние",
                    Price = (decimal)500 },
                new Product
                {
                    Id = 2,
                    Productname = "Стремянка",
                    Description = "2 метра в высоту",
                    Price = (decimal)1000
                },
                new Product
                {
                    Id = 3,
                    Productname = "Велосипед",
                    Description = "Детский велосипед, с 3-мя колесами",
                    Price = (decimal)2000
                },
                new Product
                {
                    Id = 4,
                    Productname = "Катер",
                    Description = "",
                    Price = (decimal)100000
                },
                new Product
                {
                    Id = 5,
                    Productname = "Скакалка",
                    Description = "Скакунка",
                    Price = (decimal)500
                },
                new Product
                {
                    Id = 6,
                    Productname = "Телефон Samsung",
                    Description = "Samsung A20",
                    Price = (decimal)15000
                }
            };

            return lst;
        }

        public static IEnumerable<Advertisement> GetAdvertisementsFakeData()
        {
            var lst = new List<Advertisement>()
            {
                        new Advertisement
                        {
                            Id = 1,
                            DateCreated = DateTime.UtcNow
                        },
                        new Advertisement
                        {
                            Id = 2,
                            DateCreated = DateTime.UtcNow
                        },
                        new Advertisement
                        {
                            Id = 3,
                            DateCreated = DateTime.UtcNow
                        }, new Advertisement
                        {
                            Id = 4,
                            DateCreated = DateTime.UtcNow
                        }, new Advertisement
                        {
                            Id = 5,
                            DateCreated = DateTime.UtcNow
                        }
            };

            return lst;
        }
    }
}
