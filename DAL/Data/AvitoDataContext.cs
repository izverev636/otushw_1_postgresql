﻿using System;
using Core.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data
{
    public class AvitoDataContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Advertisement> Advertisements { get; set; }

        public DbSet<AdvertisementHasProduct> AdvertisementHasProducts { get; set; }

        public AvitoDataContext(DbContextOptions<AvitoDataContext> options) : base (options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }                        

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<AdvertisementHasProduct>()
        //        .Property(ahp => ahp.Id)
        //        .ValueGeneratedOnAdd();

        //    modelBuilder.Entity<User>().HasData(
        //    new User
        //    {
        //        Id = 1,
        //        Firstname = "Ivan",
        //        Lastname = "Ivanov",
        //        Age = 25,
        //        Login = "ivan.invanov"
        //    },
        //    new User
        //    {
        //        Id = 2,
        //        Firstname = "Alex",
        //        Lastname = "Kozlov",
        //        Age = 42,
        //        Login = "alex.koz"
        //    },
        //    new User
        //    {
        //        Id = 3,
        //        Firstname = "Roma",
        //        Lastname = "Orlov",
        //        Age = 14,
        //        Login = "orelRoman2004"
        //    },
        //    new User
        //    {
        //        Id = 4,
        //        Firstname = "Albert",
        //        Lastname = "Petrov",
        //        Age = 30,
        //        Login = "ScruDriwer22"
        //    },
        //    new User
        //    {
        //        Id = 5,
        //        Firstname = "Irina",
        //        Lastname = "Are",
        //        Age = 65,
        //        Login = "OldWoman65"
        //    });

        //    modelBuilder.Entity<Product>().HasData(
        //        new  {
        //            Id = 1,
        //            Productname = "Старый зонтик",
        //            Description = "Зеленый зонтик в хорошем состояние",
        //            Price = (decimal)500 },
        //        new 
        //        {
        //            Id = 2,
        //            Productname = "Стремянка",
        //            Description = "2 метра в высоту",
        //            Price = (decimal)1000
        //        },
        //        new
        //        {
        //            Id = 3,
        //            Productname = "Велосипед",
        //            Description = "Детский велосипед, с 3-мя колесами",
        //            Price = (decimal)2000
        //        },
        //        new
        //        {
        //            Id = 4,
        //            Productname = "Катер",
        //            Description = "",
        //            Price = (decimal)100000
        //        },
        //        new
        //        {
        //            Id = 5,
        //            Productname = "Скакалка",
        //            Description = "Скакунка",
        //            Price = (decimal)500
        //        },
        //        new
        //        {
        //            Id = 6,
        //            Productname = "Телефон Samsung",
        //            Description = "Samsung A20",
        //            Price = (decimal)15000
        //        });

        //    modelBuilder.Entity<Advertisement>().HasData(
        //        new
        //        {
        //            Id = 1,
        //            UserId = 1,
        //            DateCreated = DateTime.UtcNow
        //        },
        //        new
        //        {
        //            Id = 2,
        //            UserId = 3,
        //            DateCreated = DateTime.UtcNow
        //        },
        //        new
        //        {
        //            Id = 3,
        //            UserId = 4,
        //            DateCreated = DateTime.UtcNow
        //        }, new
        //        {
        //            Id = 4,
        //            UserId = 2,
        //            DateCreated = DateTime.UtcNow
        //        }, new
        //        {
        //            Id = 5,
        //            UserId = 1,
        //            DateCreated = DateTime.UtcNow
        //        });

        //    modelBuilder.Entity<AdvertisementHasProduct>().HasData(
        //        new
        //        {
        //            Id = 1,
        //            AdvertisementId = 1,
        //            ProductId = 1
        //        },
        //        new
        //        {
        //            Id = 2,
        //            AdvertisementId = 1,
        //            ProductId = 2
        //        },
        //        new
        //        {
        //            Id = 3,
        //            AdvertisementId = 2,
        //            ProductId = 3
        //        }, new
        //        {
        //            Id = 4,
        //            AdvertisementId = 3,
        //            ProductId = 1
        //        }, new
        //        {
        //            Id = 5,
        //            AdvertisementId = 4,
        //            ProductId = 6
        //        });
        //}
    }
}

