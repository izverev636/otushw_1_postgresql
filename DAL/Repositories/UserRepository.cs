﻿using System;
using Core;
using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository: IRepository<User>
    {
        private readonly AvitoDataContext _avitoDataContext;

        public UserRepository(AvitoDataContext dataContext)
        {
            _avitoDataContext = dataContext;
        }

        public void AddAsync(User Entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteAsync(User entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _avitoDataContext.Users.ToListAsync();
        }

        public Task<User> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void SaveChangesAsync()
        {
            _avitoDataContext.SaveChangesAsync();
        }
    }
}

