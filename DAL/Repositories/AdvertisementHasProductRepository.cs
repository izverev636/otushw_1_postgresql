﻿using System;
using Core;
using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class AdvertisementHasProductRepository: IRepository<AdvertisementHasProduct>
    {
        private readonly AvitoDataContext _avitoDataContext;
        
        public AdvertisementHasProductRepository(AvitoDataContext dataContext)
        {
            _avitoDataContext = dataContext;
        }

        public void AddAsync(AdvertisementHasProduct Entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteAsync(AdvertisementHasProduct entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AdvertisementHasProduct>> GetAllAsync()
        {
            return await _avitoDataContext.AdvertisementHasProducts.Include(ahp => ahp.Advertisement).Include(ahp => ahp.Product).ToListAsync();
        }

        public Task<AdvertisementHasProduct> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void SaveChangesAsync()
        {
            _avitoDataContext.SaveChangesAsync();
        }
    }
}

