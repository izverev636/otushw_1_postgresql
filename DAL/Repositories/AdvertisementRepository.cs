﻿using System;
using Core;
using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class AdvertisementRepository: IRepository<Advertisement>
    {
        private readonly AvitoDataContext _avitoDataContext;

        public AdvertisementRepository(AvitoDataContext dataContext)
        {
            _avitoDataContext = dataContext;
        }

        public void AddAsync(Advertisement Entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteAsync(Advertisement entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Advertisement>> GetAllAsync()
        {
            return await _avitoDataContext.Advertisements.ToListAsync();
        }

        public Task<Advertisement> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void SaveChangesAsync()
        {
            _avitoDataContext.SaveChangesAsync();
        }
    }
}

