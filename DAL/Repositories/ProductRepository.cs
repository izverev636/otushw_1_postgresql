﻿using Core;
using Core.Models;
using DAL.Data;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ProductRepository: IRepository<Product>
    {
        private readonly AvitoDataContext _avitoDataContext;

        public ProductRepository(AvitoDataContext dataContext)
        {
            _avitoDataContext = dataContext;
        }

        public async void AddAsync(Product entity)
        {
            await _avitoDataContext.Products.AddAsync(entity);
        }

        public void DeleteAsync(Product entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            return await _avitoDataContext.Products.ToListAsync();
        }

        public Task<Product> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void SaveChangesAsync()
        {
            _avitoDataContext.SaveChangesAsync();
        }
    }
}

